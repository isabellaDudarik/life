package com.example.life;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    public static final String ZODIAC = "zodiac";
    public static final String NAME = "";
    public static final String TIME_OF_LIFE = "";
    public static final String DAYS = "";
    public static final String YEARS = "";

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.dob)
    EditText dob;
    @BindView(R.id.button)
    Button button;

    String zodiac;
    long timeOfLifeInmillis;
    long days;
    double years;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        button.setEnabled(true);

    }

    @OnTextChanged(R.id.dob)
    public void chekdob(CharSequence s) {
        String[] numbers = s.toString().split("/");

        Calendar birthDay = Calendar.getInstance();
        birthDay.set(Calendar.DAY_OF_MONTH, Integer.parseInt(numbers[0]));
        birthDay.set(Calendar.MONTH, Integer.parseInt(numbers[1]));
        birthDay.set(Calendar.YEAR, Integer.parseInt(numbers[2]));

        Calendar today = Calendar.getInstance();
        timeOfLifeInmillis = today.getTimeInMillis() - birthDay.getTimeInMillis();


        String zodiacSign = String.valueOf(numbers[1]);
        switch (zodiacSign) {
            case ("1"):
                zodiac = "Goat-Horned";
                break;
            case ("2"):
                zodiac = "The Water-Bearer";
                break;
            case ("3"):
                zodiac = "The Fishes";
                break;
            case ("4"):
                zodiac = "The Ram";
                break;
            case ("5"):
                zodiac = "The Bull";
                break;
            case ("6"):
                zodiac = "The Twins";
                break;
            case ("7"):
                zodiac = "The Crab";
                break;
            case ("8"):
                zodiac = "The Lion";
                break;
            case ("9"):
                zodiac = "The Maiden";
                break;
            case ("10"):
                zodiac = "The Scales";
                break;
            case ("11"):
                zodiac = "The Scorpion";
                break;
            case ("12"):
                zodiac = "The (Centaur) Archer";
                break;
        }
//        if (!(dob == null) && !(name == null)) {
//            button.setEnabled(true);
//        }
    }

    public void setInformation() {
        days = timeOfLifeInmillis / (24 * 60 * 60 * 1000);
        years = days / 365.25;
    }

    @OnClick(R.id.button)
    public void onButtonClick() {
        Intent intent = new Intent(this, Information.class);
        intent.putExtra(ZODIAC, zodiac);
        intent.putExtra(NAME, name.getText().toString());
        intent.putExtra(TIME_OF_LIFE, timeOfLifeInmillis);
        intent.putExtra(DAYS, days);
        intent.putExtra(YEARS, years);
        startActivity(intent);
    }


}
