package com.example.life;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.life.MainActivity.DAYS;
import static com.example.life.MainActivity.NAME;
import static com.example.life.MainActivity.TIME_OF_LIFE;
import static com.example.life.MainActivity.YEARS;
import static com.example.life.MainActivity.ZODIAC;

public class Information extends AppCompatActivity {
    @BindView(R.id.ai_name)
    TextView name;
    @BindView(R.id.ai_years)
    TextView years;
    @BindView(R.id.ai_days)
    TextView days;
    @BindView(R.id.ai_seconds)
    TextView seconds;
    @BindView(R.id.ai_zodiac)
    TextView zodiac;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        ButterKnife.bind(this);


        name.setText(getIntent().getStringExtra(NAME));
        years.setText(String.format("You're %s years old", getIntent().getStringExtra(YEARS)));
        days.setText(String.format("You're lived %s days", getIntent().getStringExtra(DAYS)));
        zodiac.setText(String.format("Your zodiac is %s", getIntent().getStringExtra(ZODIAC)));
        seconds.setText(String.format("At the time of opening statistics you turned %s seconds", getIntent().getStringExtra(TIME_OF_LIFE)));


    }


}
